const callback1 = require('./callback1.js');
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');

const getData = function ([requiredBoardName, requiredListName, requiredCardName], [boardsData, listsData, cardsData]) {
    const names = arguments[0];
    const data = arguments[1];

    const resultCheckNames = Array.isArray(names) && names.reduce((namesAccumulator, namesCurrentValue) => {
        return namesAccumulator = namesAccumulator && (typeof (namesCurrentValue) == 'string');
    }, true);

    const resultCheckData = Array.isArray(data);

    if ((resultCheckNames) && (resultCheckData)) {
        setTimeout(() => {
            const boardInfo = boardsData.filter((currentValue) => {
                return currentValue['name'] == requiredBoardName;
            });
            if (boardInfo.length != 0) {
                boardInfo.map((currentBoard) => {
                    callback1.getBoardInfo(currentBoard['id'], boardsData, (err, data) => {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('\n Boards Data is as follows: ');
                            console.log(data);
                        };
                    });
                });
            }
            else {
                console.error(new Error('Board Name not found in boards to find board'));
            };

            const listInfo = boardsData.filter((currentValue) => {
                return currentValue['name'] == requiredListName;
            });
            if (listInfo.length != 0) {
                listInfo.map((currentList) => {
                    callback2.getListInfo(currentList['id'], listsData, (err, data) => {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('\n Lists Data is as follows: ');
                            console.log(data);
                        };
                    });
                });
            }
            else {
                console.error(new Error('Board Name not found in boards to find list'));
            };


            const listsDataArray = Object.entries(listsData);

            const nestedArray = listsDataArray.map((currentValue) => {
                return currentValue[1];
            });
            function flattenArray(result, nextArray) {
                nextArray.map((currentValueFlatten) => {
                    if (Array.isArray(currentValueFlatten)) {
                        flattenArray(result, currentValueFlatten);
                    }
                    else {
                        result.push(currentValueFlatten);
                    };
                });
                return result;
            };

            const flattenArrayResult = flattenArray([], nestedArray);
            const cardInfo = flattenArrayResult.filter((currentValue) => {
                return currentValue['name'] === requiredCardName;
            });

            if (cardInfo.length != 0) {
                cardInfo.map((currentCard) => {
                    callback3.getCardsInfo(currentCard['id'], cardsData, (err, data) => {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('\n Cards Data is as follows: ');
                            console.log(data);
                        };
                    });
                });
            }
            else {
                console.error(new Error('List Name not found in list'));
            };
        }, 2 * 1000);
    }
    else {
        console.error(new Error('Function arguments are incorrect'));
    };
};


module.exports = { getData };