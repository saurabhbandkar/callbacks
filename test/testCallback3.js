const callback3 = require('../callback3.js');
const cardsData = require('../cards.json');
const requiredTestId1 = 'qwsa221';
const requiredTestId2 = 'ghyu556';
const requiredTestId3 = 'azxs123';
const requiredTestId4 = 45;

//Test1: Correct ID1
callback3.getCardsInfo(requiredTestId1, cardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test2: Incorrect ID1
callback3.getCardsInfo(requiredTestId2, cardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test3: Correct ID2
callback3.getCardsInfo(requiredTestId3, cardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test4: Incorrect datatype
callback3.getCardsInfo(requiredTestId4, cardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});