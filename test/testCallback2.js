const callback2 = require('../callback2.js');
const listsData = require('../lists.json');
const requiredTestId1 = 'mcu453ed';
const requiredTestId2 = 'abc122dc';
const requiredTestId3 = 'unavailable';
const requiredTestId4 = 45;

//Test1: Correct ID1
callback2.getListInfo(requiredTestId1, listsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test2: Correct ID2
callback2.getListInfo(requiredTestId2, listsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test3: Incorrect ID1
callback2.getListInfo(requiredTestId3, listsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test4: Incorrect datatype
callback2.getListInfo(requiredTestId4, listsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});