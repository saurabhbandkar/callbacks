const callback4 = require ('../callback4.js');
const boardsData = require('../boards.json');
const listsData = require('../lists.json');
const cardsData = require('../cards.json');
const requiredBoardName = 'Thanos';
const requiredListName = 'Thanos';
const requiredCardName = 'Mind';

//Test1: Correct ID1
callback4.getData([requiredBoardName, requiredListName, requiredCardName], [boardsData, listsData, cardsData]);

//Test2: Incorrect ID1
callback4.getData([requiredBoardName, 'notThanos', requiredCardName], [boardsData, listsData, cardsData]);