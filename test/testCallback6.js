const callback6 = require ('../callback6.js');
const boardsData = require('../boards.json');
const listsData = require('../lists.json');
const cardsData = require('../cards.json');
const requiredBoardName = 'Thanos';
const requiredListName = 'Thanos';

//Test1: Correct ID1
callback6.getData([requiredBoardName, requiredListName], [boardsData, listsData, cardsData]);

//Test2: Incorrect ID1
callback6.getData([requiredBoardName, 'notThanos'], [boardsData, listsData, cardsData]);