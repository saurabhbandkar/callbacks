const callback1 = require('../callback1.js');
const boardsData = require('../boards.json');
const requiredTestId1 = 'mcu453ed';
const requiredTestId2 = 'abc122dc';
const requiredTestId3 = 'unavailable';
const requiredTestId4 = 45;

//Test1: Correct ID1
callback1.getBoardInfo(requiredTestId1, boardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test2: Correct ID2
callback1.getBoardInfo(requiredTestId2, boardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test3: Incorrect ID1
callback1.getBoardInfo(requiredTestId3, boardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});

//Test4: Incorrect datatype
callback1.getBoardInfo(requiredTestId4, boardsData, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    };
});