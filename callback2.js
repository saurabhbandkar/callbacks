const getListInfo = function (requiredBoardId, listsData, callback) {
    if ((typeof (requiredBoardId) == 'string') && (typeof (listsData) == 'object') && (typeof (callback) == 'function')) {
        setTimeout(() => {
            const listsDataArray = Object.entries(listsData);

            const listsInfo = listsDataArray.filter((currentValue) => {
                return currentValue[0] == requiredBoardId;
            });
            if (listsInfo.length != 0) {
                listsInfo.map((currentList) => {
                    callback(undefined, currentList);
                });
            }
            else {
                callback(new Error('Board ID not found'), undefined);
            };
        }, 2 * 1000);
    }
    else {
        callback(new Error('Function arguments are incorrect'), undefined);
    };
};


module.exports = { getListInfo };