const getCardsInfo = function (requiredListId, cardsData, callback) {
    if ((typeof (requiredListId) == 'string') && (typeof (cardsData) == 'object') && (typeof (callback) == 'function')) {
        setTimeout(() => {
            const cardsDataArray = Object.entries(cardsData);

            const cardsInfo = cardsDataArray.filter((currentValue) => {
                return currentValue[0] == requiredListId;
            });
            if (cardsInfo.length != 0) {
                cardsInfo.map((currentCard) => {
                    callback(undefined, currentCard);
                });
            }
            else {
                callback(new Error('List ID not found'), undefined);
            };
        }, 2 * 1000);
    }
    else {
        callback(new Error('Function arguments are incorrect'), undefined);
    };
};


module.exports = { getCardsInfo };