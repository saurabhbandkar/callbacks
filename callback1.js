const getBoardInfo = function (requiredBoardId, boardsData, callback) {
    if ((typeof (requiredBoardId) == 'string') && (Array.isArray(boardsData)) && (typeof (callback) == 'function')) {
        setTimeout(() => {
            const boardInfo = boardsData.filter((currentValue) => {
                return currentValue['id'] == requiredBoardId;
            });
            if (boardInfo.length != 0) {
                boardInfo.map((currentBoard) => {
                    callback(undefined, currentBoard);
                });
            }
            else {
                callback(new Error('Board ID not found'), undefined);
            };
        }, 2 * 1000);
    }
    else {
        callback(new Error('Function arguments are incorrect'), undefined);
    };
};


module.exports = { getBoardInfo };